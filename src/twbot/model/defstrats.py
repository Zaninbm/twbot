from twbot.model.strategy import Strategy


class DefensiveStrategy(Strategy):

    def __init__(self):
        super(DefensiveStrategy, self).__init__()
        self.unit_targets = {
                "spear": 6000,
                "sword": 6000,
                "archer": 6000,
                "heavy": 500,
                "spy": 300,
            }

    def get_unit_to_train(self, current_units):
        targets = {key:self.unit_targets[key] for key in ["spear", "sword", "archer"]}
        return self._get_unit_to_train(current_units=current_units, targets=targets)

    def get_stable_unit(self, current_units):
        targets = {key:self.unit_targets[key] for key in ["heavy", "spy"]}
        return self._get_unit_to_train(current_units=current_units, targets=targets)

