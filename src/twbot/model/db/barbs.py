from dataclasses import dataclass, field
from datetime import datetime
from enum import IntEnum, auto, unique
from typing import Dict, Optional

from sqlalchemy.orm.session import Session
from sqlalchemy.sql.schema import Column, MetaData, Table
from sqlalchemy.sql.sqltypes import Enum, Integer, JSON, String, TIMESTAMP
from sqlalchemy.orm import mapper


@unique
class BarbStatus(IntEnum):
    GOOD_TO_GO = auto()
    NEEDS_SCOUTING = auto()
    SCOUT_ON_THE_WAY = auto()
    NEEDS_RAMMING = auto()
    RAM_ON_THE_WAY = auto()
    HOLD_OFF = auto()


@dataclass
class Barbarian:
    id: Optional[int] = None
    choords: str = field(default_factory=str)
    buildings: Dict[str, int] = field(default_factory=dict)
    next_arrival: datetime = field(default_factory=lambda: datetime.fromtimestamp(86400))
    barb_status: BarbStatus = field(default_factory=lambda: BarbStatus.GOOD_TO_GO)
    points: int = field(default_factory=int)


class BarbController:
    metadata = MetaData()
    barb_table = Table(
        "barbs",
        metadata,
        Column("id", Integer, primary_key=False, autoincrement=True),
        Column("choords", String(7), primary_key=True),
        Column("buildings", JSON),
        Column("next_arrival", TIMESTAMP),
        Column("barb_status", Enum(BarbStatus)),
        Column("points", Integer),
    )
    mapper(Barbarian, barb_table)

    def __init__(self, session: Session) -> None:
        self.session = session
        self.metadata.create_all(session.get_bind())

    def update_barb(self, barb: Barbarian):
        self.session.expunge_all()
        self.session.add(barb)
        self.session.commit()

    def get_barb(self, choords):
        barb = (
            self.session.query(Barbarian)
            .filter(self.barb_table.c.choords == choords)
            .first()
        )
        return barb

    def get_all_barbs(self):
        return self.session.query(Barbarian).all()
