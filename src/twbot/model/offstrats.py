from .strategy import Strategy


class OffensiveStrategy(Strategy):
    def __init__(self):
        super(OffensiveStrategy, self).__init__()
        self.unit_targets = {
                "axe": 6000,
                "light": 2500,
                "marcher": 500,
                "ram": 500
            }

    def get_unit_to_train(self, current_units):
        targets = {key:self.unit_targets[key] for key in ["axe"]}
        return self._get_unit_to_train(current_units=current_units, targets=targets)

    def get_stable_unit(self,current_units):
        targets = {key:self.unit_targets[key] for key in ["light", "marcher"]}
        return self._get_unit_to_train(current_units=current_units, targets=targets)


class EarlyOffensiveStrategy(OffensiveStrategy):
    def __init__(self):
        super(EarlyOffensiveStrategy, self).__init__()
        self.num_spears = 500

    def get_unit_to_train(self, current_units):
        if current_units['spear'] < self.num_spears:
            return "spear"
        return super().get_unit_to_train(current_units)
